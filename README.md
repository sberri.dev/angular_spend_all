# Installation d'un projet Angular 

## Prérequis 

1. Avant de commencer l'installation d'Angular, il faut d'abord vérifier si la version npm est correctement installée avec la commande `npm --version`
 1. Il est necessaire de mettre à jour npm si la version npm est ancienne (en dessous de 8.5.0) 

 ## Initiation projet Angular 

1. Ouvrez votre terminal et exécutez la commande suivante pour installer Angular sur votre machine : `npm init @angular nom-du-projet`et remplacer `nom-du-projet` par le nom que vous souhaitez donner au projet.

2. Pour pouvoir naviguer sur le répertoir du projet, executez cette commande : `cd nom-du-projet`

3. Executez cette commande : `npm start` pour démarrer le serveur de développement.

## Démarrer le serveur

1. Executez cette commande : `npm start` pour démarrer le serveur de développement.

2. Accédez à cette URL indiquée dans le terminal `http://localhost:4200/` L'application se rechargera automatiquement

# Installation d'un projet existant 

## Cloner le projet 

1. Récupérer le lien SSH de projet que vous voulez cloner depuis un dépôt Git
2. Ouvrez votre terminal et naviguez jusqu'au répertoire où vous souhaitez cloner le projet. Ensuite, exécutez la commande suivante pour cloner le projet : `git clone <URL-du-dépôt>` Remplacez `<URL-du-dépôt>` par l'URL du dépôt Git où se trouve le projet 

## Installation des dépendances

1. Une fois le projet cloné, naviguez dans  le répertoir du projet en question, executez cette commande : `cd nom-du-projet`
2. Exécutez la commande suivante pour installer les dépendances du projet : `npm install`
3. Accédez à cette URL indiquée dans le terminal `http://localhost:4200/` L'application se rechargera automatiquement


